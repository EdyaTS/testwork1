from selenium import webdriver
import allure
import secrets

def setup_module(module):
    global driver
    driver = webdriver.Chrome(executable_path='./chromedriver.exe')

def teardown_module():
    print('quit module')
    global driver
    driver.quit()

@allure.title('Главная страница')
def test_netpeak_main():

    with allure.step('Открываем сайт Netpeak'):
        driver.get('https://netpeak.ua/')

    with allure.step('Нажимаем на кнопку "О нас" и в выпадающем списке нажимаем "Команда"'):
        drop_down_list = driver.find_element_by_xpath('//*[@id="rec278727844"]/div/div/div/div[1]/div/nav/div[1]/div[1]/ul/li[3]').click()
        team_button = driver.find_element_by_link_text('Команда').click()

    print('Сайт открыт')
    ...

@allure.title('Страница работа в Нетпик')
def test_netpek_job():

    with allure.step('Нажать кнопку "Стать частью команды" и убедится что в новой вкладке открылась страница Работа в Нетпик'):
        driver.maximize_window()
        career_button = driver.find_element_by_link_text('Стать частью команды').click()

        tabs = driver.window_handles
        driver.switch_to.window(tabs[1])

        if driver.title == 'Работа в Netpeak: обращение руководителя, видео и презентация о карьере в Нетпик':
            print('Страница "Работа в Netpeak" открыта')
        else:
            print('Страница не найдена')

    with allure.step('Убедится что на странице есть кнопка "Я хочу работать в Netpeak" и на нее можно кликнуть.'):
        btn_green = driver.find_element_by_link_text("Я хочу работать в Netpeak")
        print(btn_green.is_displayed())
        print(btn_green.is_enabled())

@allure.title('Страница личного кабинета')
def test_netpeak_login():
    print('Страница личного кабинета открыта')
    with allure.step('Вернутся на предыдущую вкладку и нажать кнопку "Личный кабинет"'):
        tabs = driver.window_handles
        driver.switch_to.window(tabs[0])

        btn_login = driver.find_element_by_link_text('Личный кабинет').click()

    with allure.step('На странице личного кабинета заполнить Логин и Пароль случайными данными.'):
        tabs = driver.window_handles
        driver.switch_to.window(tabs[2])

        username = secrets.token_urlsafe(10)
        password = secrets.token_urlsafe(10)

        input_login = driver.find_element_by_id('login').send_keys(username)
        input_pass = driver.find_element_by_id('password').send_keys(password)
        btn_enter = driver.find_element_by_xpath('//*[@id="loginForm"]/div[5]/button')

    with allure.step('Проверить что кнопка "Войти" не доступна.'):
        print(btn_enter.is_enabled())
        ...

    with allure.step('Отметить чекбокс "Авторизируясь, вы соглашаетесь с Политикой конфиденциальности"'):
        checkbox = driver.find_element_by_xpath('//*[@id="loginForm"]/div[4]/div/md-checkbox/div[1]').click()
        ...

    with allure.step('Нажать на кнопку войти и проверить наличие нотификации о неправильном логине или пароле'):
        btn_enter.click()
        fail_box = driver.find_element_by_xpath('/html/body/md-toast/div')
        print(fail_box.is_displayed())

    with allure.step('Проверить что Логин и Пароль подсветились красным цветом'):
        login_rgba = driver.find_element_by_xpath('//*[@id="loginForm"]/div[1]/md-input-container/label').value_of_css_property('color')
        if login_rgba == "rgba(221, 44, 0, 1)":
            print('Логин - красный')
        else:
            print('Логин - не красный')

        pass_rgba = driver.find_element_by_xpath('//*[@id="loginForm"]/div[2]/md-input-container/label').value_of_css_property('color')
        if pass_rgba == "rgba(221, 44, 0, 1)":
            print('Пароль - красный')
        else:
            print('Пароль - не красный')
    ...
